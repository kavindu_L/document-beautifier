import ButtonGroup from "./ButtonGroup";

function EditLeft({
  handleAlignment,
  handleFont,
  handleTextColor,
  handleTextSize,
  textSize,
  textColor,
  textAlignment,
  textFont
}) {
  const textAlignmentButtonValues = ["left", "center", "right", "justify"];

  const fonFamilyButtonValues = [
    "serif",
    "sans-serif",
    "Verdana",
    "Courier New"
  ];
  const fonFamilyButtonLabels = [
    "serif",
    "sans-serif",
    "verdana",
    "courier new"
  ];

  const textColorButtonValues = ["black", "gray", "red", "blue"];
  const textSizeButtonValues = ["10px", "20px", "30px", "40px"];

  return (
    <div className="container text-start">
      {/* <h4>Edit</h4> */}
      <ButtonGroup
        onClickFunction={handleAlignment}
        title="Text Alignment"
        stateVariable={textAlignment}
        buttonValues={textAlignmentButtonValues}
        buttonLabels={textAlignmentButtonValues}
      />

      <ButtonGroup
        onClickFunction={handleFont}
        title="Font Family"
        stateVariable={textFont}
        buttonValues={fonFamilyButtonValues}
        buttonLabels={fonFamilyButtonLabels}
      />

      <ButtonGroup
        onClickFunction={handleTextColor}
        title="Text Color"
        stateVariable={textColor}
        buttonValues={textColorButtonValues}
        buttonLabels={textColorButtonValues}
      />

      <ButtonGroup
        onClickFunction={handleTextSize}
        title="Text Size"
        stateVariable={textSize}
        buttonValues={textSizeButtonValues}
        buttonLabels={textSizeButtonValues}
      />
    </div>
  );
}

export default EditLeft;
