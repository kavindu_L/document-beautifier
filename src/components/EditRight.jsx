import ButtonGroup from "./ButtonGroup";
import jsPDF from "jspdf";

function EditRight({
  textWeight,
  handleTextWeight,
  textCase,
  handleTextCase,
  textEffect,
  handleTextEffect,

  handleResetButton,
  beautifyState
}) {
  const textWeightButtonValues = ["normal", "bold", "bolder", "lighter"];
  const textCaseButtonValues = ["uppercase", "lowercase", "capitalize"];
  const textEffectButtonValues = [
    "2px 2px",
    "1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue",
    "2px 2px 5px red",
    "0 0 3px #FF0000"
  ];
  const textEffectButtonLabels = ["shadow", "dark blue", "red", "red+neon"];

  const handleGeneratePdf = () => {
    const doc = new jsPDF({
      format: "a4",
      unit: "px"
    });

    // Adding the fonts.
    // doc.setFont('Inter-Regular', 'normal');

    let element = document.querySelector(".beautify-text");

    doc.html(element, {
      async callback(doc) {
        await doc.save("document");
      }
    });
  };

  return (
    <div className="container text-start">
      {/* <h4>Edit and Save</h4> */}

      <ButtonGroup
        onClickFunction={handleTextWeight}
        title="Text Weight"
        stateVariable={textWeight}
        buttonValues={textWeightButtonValues}
        buttonLabels={textWeightButtonValues}
      />

      <ButtonGroup
        onClickFunction={handleTextCase}
        title="Text Case"
        stateVariable={textCase}
        buttonValues={textCaseButtonValues}
        buttonLabels={textCaseButtonValues}
      />

      <ButtonGroup
        onClickFunction={handleTextEffect}
        title="Text Effect"
        stateVariable={textEffect}
        buttonValues={textEffectButtonValues}
        buttonLabels={textEffectButtonLabels}
      />

      <button
        className="btn btn-sm btn-success pdf-btn"
        onClick={handleGeneratePdf}
      >
        Generate Pdf
      </button>
      <br />
      <button
        className="btn btn-sm btn-danger exit-btn"
        onClick={handleResetButton}
        hidden={!beautifyState}
      >
        Exit Editing
      </button>
    </div>
  );
}

export default EditRight;
