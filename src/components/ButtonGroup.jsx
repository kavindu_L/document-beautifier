function ButtonGroup({
  onClickFunction,
  title,
  stateVariable,
  buttonValues,
  buttonLabels
}) {
  return (
    <div
      className="mt-3 overflow-auto btn-group-wrapper"
      onClick={onClickFunction}
    >
      <h5 className="btn-group-title">{title}</h5>
      {/* <hr /> */}
      <div className="btn-group" role="group" aria-label="Basic example">
        {buttonValues[0] && (
          <button
            className={
              "btn btn-sm btn-light " +
              (stateVariable === buttonValues[0] ? "active" : "")
            }
            value={buttonValues[0]}
          >
            {buttonLabels[0]}
          </button>
        )}

        {buttonValues[1] && (
          <button
            className={
              "btn btn-sm btn-light " +
              (stateVariable === buttonValues[1] ? "active" : "")
            }
            value={buttonValues[1]}
          >
            {buttonLabels[1]}
          </button>
        )}

        {buttonValues[2] && (
          <button
            className={
              "btn btn-sm btn-light " +
              (stateVariable === buttonValues[2] ? "active" : "")
            }
            value={buttonValues[2]}
          >
            {buttonLabels[2]}
          </button>
        )}

        {buttonValues[3] && (
          <button
            className={
              "btn btn-sm btn-light " +
              (stateVariable === buttonValues[3] ? "active" : "")
            }
            value={buttonValues[3]}
          >
            {buttonLabels[3]}
          </button>
        )}
      </div>
      <br />

      <button
        className={
          "mt-1 btn btn-sm btn-light " + (stateVariable === "" ? "active" : "")
        }
        value=""
      >
        none
      </button>
    </div>
  );
}

export default ButtonGroup;
