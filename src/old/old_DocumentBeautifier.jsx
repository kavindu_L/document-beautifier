import { useState, useRef } from "react";
import EditLeft from "./EditLeft";
import EditRight from "./EditRight";
// import Pdf from "react-to-pdf";

function DocumentBeautifier() {
  const ref = useRef();

  const [showSideBars, setShowSideBars] = useState(false);
  const [beautifyState, setBeautifyState] = useState(false);
  const [textAreaValue, setTextAreaValue] = useState("");

  const [textAlignment, setTextAlignment] = useState("");
  const [textFont, setTextFont] = useState("");
  const [textColor, setTextColor] = useState("");
  const [textSize, setTextSize] = useState("");
  const [textWeight, setTextWeight] = useState("");
  const [textCase, setTextCase] = useState("");
  const [textEffect, setTextEffect] = useState("");

  const styles = {
    whiteSpace: "pre-line",
    textAlign: textAlignment,
    fontFamily: textFont,
    color: textColor,
    fontSize: textSize,
    padding: "10px",
    height: "710px",
    fontWeight: textWeight,
    textTransform: textCase,
    textShadow: textEffect
  };

  function handleAddButton() {
    setBeautifyState(true);
    setShowSideBars(true);
  }

  function handleResetButton() {
    setShowSideBars(false);
    setBeautifyState(false);
    setTextAreaValue("");
  }

  function handleAlignment(event) {
    setTextAlignment(event.target.value);
  }

  function handleFont(event) {
    setTextFont(event.target.value);
  }

  function handleTextColor(event) {
    setTextColor(event.target.value);
  }

  function handleTextSize(event) {
    setTextSize(event.target.value);
  }

  function handleTextWeight(event) {
    setTextWeight(event.target.value);
  }

  function handleTextCase(event) {
    setTextCase(event.target.value);
  }

  function handleTextEffect(event) {
    setTextEffect(event.target.value);
  }

  return (
    <div className="container mt-5">
      <div className="container">
        <div className="d-flex justify-content-between text-center">
          <div
            className={
              "col col-3 rounded text-bg-" + (beautifyState ? "secondary" : "")
            }
          >
            {showSideBars ? (
              <EditLeft
                handleAlignment={handleAlignment}
                handleFont={handleFont}
                handleTextColor={handleTextColor}
                handleTextSize={handleTextSize}
                textSize={textSize}
                textColor={textColor}
                textAlignment={textAlignment}
                textFont={textFont}
              />
            ) : (
              ""
            )}
          </div>

          <div
            className={
              "col col-6 ms-3 me-3 rounded " +
              (beautifyState && "text-bg-light")
            }
          >
            <h2 className="text-bg-dark">Document Beautifier</h2>

            <div className="d-flex flex-column rounded">
              <div ref={ref} className="overflow-auto">
                {beautifyState ? (
                  <p style={styles}>{textAreaValue}</p>
                ) : (
                  <textarea
                    placeholder="type something here ..."
                    value={textAreaValue}
                    onChange={e => setTextAreaValue(e.target.value)}
                    className="form-control"
                    cols="10"
                    rows="30"
                  />
                )}
              </div>

              <div className="my-2">
                <button
                  className="btn btn-dark"
                  onClick={handleAddButton}
                  hidden={beautifyState}
                >
                  Beatify
                </button>

                {/* {beautifyState && (
                  <Pdf targetRef={ref} filename="beautified-document.pdf">
                    {({ toPdf }) => (
                      <button className="btn btn-dark" onClick={toPdf}>
                        Generate Pdf
                      </button>
                    )}
                  </Pdf>
                )} */}
              </div>
            </div>
          </div>

          <div
            className={
              "col col-3 rounded text-bg-" + (beautifyState ? "secondary" : "")
            }
          >
            {showSideBars ? (
              <EditRight
                handleTextWeight={handleTextWeight}
                textWeight={textWeight}
                textCase={textCase}
                handleTextCase={handleTextCase}
                handleTextEffect={handleTextEffect}
                textEffect={textEffect}
                handleResetButton={handleResetButton}
                beautifyState={beautifyState}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default DocumentBeautifier;
