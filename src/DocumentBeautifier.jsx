import { useState, useRef } from "react";
import { FaArrowRight } from "react-icons/fa";

import "./index.css";
import EditLeft from "./components/EditLeft";
import EditRight from "./components/EditRight";

function DocumentBeautifier() {
  const textInputRef = useRef();
  const [beautifyState, setBeautifyState] = useState(false);

  const [textAlignment, setTextAlignment] = useState("");
  const [textFont, setTextFont] = useState("");
  const [textColor, setTextColor] = useState("");
  const [textSize, setTextSize] = useState("");
  const [textWeight, setTextWeight] = useState("");
  const [textCase, setTextCase] = useState("");
  const [textEffect, setTextEffect] = useState("");

  const styles = {
    whiteSpace: "pre-line",
    textAlign: textAlignment,
    fontFamily: textFont,
    color: textColor,
    fontSize: textSize,
    padding: "10px",
    // width: "100%",
    // height: "710px",
    fontWeight: textWeight,
    textTransform: textCase,
    textShadow: textEffect
  };

  function handleGetStarted() {
    document.querySelector(".header").classList.add("started");
    document.querySelector(".main-container").style.justifyContent =
      "flex-start";
    document.querySelector(".get-started-btn").style.display = "none";
    document.querySelector(".text-area-container").style.display = "block";
    document.querySelector(".text-area-submit-btn").style.display = "block";
  }

  function handleBeautifyButton() {
    setBeautifyState(true);
  }

  function handleResetButton() {
    setBeautifyState(false);
    textInputRef.current.value = "";
    document.querySelector(".header").classList.remove("started");
    document.querySelector(".main-container").style.justifyContent = "";
    document.querySelector(".get-started-btn").style.display = "block";
    document.querySelector(".text-area-container").style.display = "none";
    document.querySelector(".text-area-submit-btn").style.display = "none";
  }

  function handleAlignment(event) {
    setTextAlignment(event.target.value);
  }

  function handleFont(event) {
    setTextFont(event.target.value);
  }

  function handleTextColor(event) {
    setTextColor(event.target.value);
  }

  function handleTextSize(event) {
    setTextSize(event.target.value);
  }

  function handleTextWeight(event) {
    setTextWeight(event.target.value);
  }

  function handleTextCase(event) {
    setTextCase(event.target.value);
  }

  function handleTextEffect(event) {
    setTextEffect(event.target.value);
  }

  return (
    <div className="app-wrapper">
      <div className="left-panel" hidden={!beautifyState}>
        <EditLeft
          handleAlignment={handleAlignment}
          handleFont={handleFont}
          handleTextColor={handleTextColor}
          handleTextSize={handleTextSize}
          textSize={textSize}
          textColor={textColor}
          textAlignment={textAlignment}
          textFont={textFont}
        />
      </div>
      <div className="main-container">
        <div className="header">Document Beautifier</div>
        <button className="get-started-btn" onClick={handleGetStarted}>
          <span className="get-started-txt">GET STARTED</span>
          <FaArrowRight />
        </button>
        <div className="text-area-container" hidden={beautifyState}>
          <textarea
            placeholder="Type something here ..."
            ref={textInputRef}
            className="text-area-input"
            hidden={beautifyState}
          />
        </div>
        <button
          className="btn btn-dark mt-3 text-area-submit-btn"
          onClick={handleBeautifyButton}
          hidden={beautifyState}
        >
          Beatify
        </button>
        <p className="beautify-text" style={styles} hidden={!beautifyState}>
          {textInputRef.current && textInputRef.current.value}
        </p>
      </div>
      <div className="right-panel" hidden={!beautifyState}>
        <EditRight
          handleTextWeight={handleTextWeight}
          textWeight={textWeight}
          textCase={textCase}
          handleTextCase={handleTextCase}
          handleTextEffect={handleTextEffect}
          textEffect={textEffect}
          handleResetButton={handleResetButton}
          beautifyState={beautifyState}
        />
      </div>
    </div>
  );
}

export default DocumentBeautifier;
