import React from "react";
import ReactDOM from "react-dom/client";
import DocumentBeautifier from "./DocumentBeautifier";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <DocumentBeautifier />
  </React.StrictMode>
);
